<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>

	<form action="/welcome" method="post">
        @csrf
        
	  	<label for="fname">First name:</label><br>
  		<input type="text" id="fname" name="fname"><br><br>
  		<label for="lname">Last name:</label><br>
  		<input type="text" id="lname" name="lname"><br><br>
  		
  		<label for="gender">Gender:</label><br>
  		<input type="radio" name="fav_language" id="male" value="Male">
  		<label for="male">Male</label><br>
  		<input type="radio" name="fav_language" id="female" value="Female">
  		<label for="female">Female</label><br>
  		<input type="radio" name="fav_language" id="other" value="Other">
  		<label for="other">Other</label><br><br>

  		<label for="nationality">Nationality:</label>
            <select name="Nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Singapura">Singapura</option>
            </select><br><br>

        <label for="language">Languange Spoken:</label><br>
        <input type="checkbox" name="indonesian" id="indonesian" value="Indonesia">
        <label for="indonesia">Indonesia</label><br>
        <input type="checkbox" name="english" id="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" name="other" id="other" value="Other">
        <label for="other">Other</label><br><br>

        <label for="bio">Bio:</label><br>
        <textarea rows = "5" cols = "40" name = "bio" id="bio"></textarea><br>

        
        <input type="submit" name="submit" value="Sign Up">
   	</form>
</body>
</html>