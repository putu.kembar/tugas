@extends('adminlte.master')

@section('content')

<div class="mt-2 ml-2">

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name = "nama" value="{{ old('nama','') }}" placeholder="masukan nama">
                    @error('nama')
                    <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name = "umur" value="{{ old('umur','') }}" placeholder="umur">
                    @error('umur')
                    <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name = "bio" value="{{ old('bio','') }}" placeholder="bio">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              </form>
            </div>

</div>

@endsection