@extends('adminlte.master')

@section('content')

<div class="mt-2 ml-2">

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Cast {{$cast->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast/{{$cast->id}}" method="POST">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name = "nama" value="{{$cast->nama}}" placeholder="masukan nama">
                    @error('nama')
                    <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name = "umur" value="{{$cast->umur}}" placeholder="umur">
                    @error('umur')
                    <div class="alert alert-danger mt-1">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name = "bio" value="{{$cast->bio}}" placeholder="bio">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href="/cast" class ="btn btn-primary">Home</a>
                </div>
              </form>
            </div>

</div>

@endsection