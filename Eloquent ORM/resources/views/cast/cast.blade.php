@extends('adminlte.master')

@section('content')
<div class="container mt-2 ml-2">
<a href="/cast/create" class="btn btn-primary m-1">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td class="d-flex" >
                            <a href="/cast/{{$value->id}}" class="btn-sm btn-info m-1">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn-sm btn-primary m-1">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn-sm btn-danger m-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="5">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
</div>

@endsection

