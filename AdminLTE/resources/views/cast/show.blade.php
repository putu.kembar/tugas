@extends('adminlte.master')

@section('content')
<div class="container ml-2">
    <h2>Cast {{$cast->id}}</h2>
    <h3>Nama : {{$cast->nama}}</h3>
    <h3>Umur : {{$cast->umur}}</h3>
    <h3>Bio : {{$cast->bio}}</h3>
    <a href="/cast" class="btn btn-primary m-1">HOME</a>
</div>
@endsection